//import fs from 'fs'
import axios from "axios";
import React from "react";
import Scheduler, {
	SchedulerData,
	ViewTypes,
	DATE_FORMAT
} from "react-big-scheduler";
import "react-big-scheduler/lib/css/style.css";
import moment from "moment";
import withDragDropContext from "./withDnDContext";

class Container extends React.Component {
	constructor(props) {
		super(props);

		_initialiseProps.call(this);

		// let schedulerData = new SchedulerData(
		//     new moment().format(DATE_FORMAT),
		//     ViewTypes.Day
		//
		let dow = new moment().day()
		let schedulerData = new SchedulerData(
			moment().subtract(new moment().day(), 'days').format(DATE_FORMAT),
			ViewTypes.Day
		);

		let resources = [
			{
				id: "0",
				name: "Monday"
			},
			{
				id: "1",
				name: "Tuesday"
			},
			{
				id: "2",
				name: "Wednesday"
			},
			{
				id: "3",
				name: "Thursday"
			},
			{
				id: "4",
				name: "Friday"
			},
			{
				id: "5",
				name: "Saturday"
			},
			{
				id: "6",
				name: "Sunday"
			}
		];

		let events = [];

		schedulerData.setResources(resources);
		this.state = {
			//data: {
			title: "",
			desc: "",
			path: "",
			//},
			viewModel: schedulerData,
			fullData: []

		};
		axios.get("/api/fullg").then((response) => {
			// handle success
			this.setState({
				fullData: response.data
			})
			// console.log('fd')
			// console.log(this.state.fullData)
		});

		axios.get("/api/datag").then(function(response) {
			events = response.data[0];
			// console.log("receved events")
			// handle success
		}).then(() => {
			// console.log(events)
			schedulerData.setEvents(events);
			// this.state.viewModel = schedulerData

			this.setState({
				viewModel: schedulerData
			});
		}).catch((e) => console.log(e))

		this.handleChangeT = this.handleChangeT.bind(this);
		this.handleChangeD = this.handleChangeD.bind(this);
		this.handleChangeP = this.handleChangeP.bind(this);
	}

	handleChangeT(event) {
		this.setState({ title: event.target.value });
		// console.log(ViewTypes.Day);
		// console.log(ViewTypes.Week);
		// console.log(ViewTypes.Month);
	}
	handleChangeD(event) {
		this.setState({ desc: event.target.value });
	}
	handleChangeP(event) {
		this.setState({ path: event.target.value });
		// console.log(event.target.value)
		this.getSDuration((m) => {alert(m)})
	}

	render() {
		const { viewModel } = this.state;
		return React.createElement(
			"div",
			null,
			React.createElement(
				"div",
				null,
				React.createElement(
					"form",
					null,
					React.createElement(
						"label",
						null,
						"Title:",
						React.createElement("input", {
							type: "text",
							value: this.state.title,
							onChange: this.handleChangeT
						}),
						"Description:",
						React.createElement("input", {
							type: "text",
							value: this.state.desc,
							onChange: this.handleChangeD
						}),
						"Path:",
						React.createElement(
							"div",
							null,
							React.createElement("input", {
								type: "file",
								value: this.state.path,
								onChange: this.handleChangeP
							})
						)
					)
				),
				"           ",
				React.createElement(Scheduler, {
					schedulerData: viewModel,
					prevClick: this.prevClick,
					nextClick: this.nextClick,
					onSelectDate: this.onSelectDate,
					onViewChange: this.onViewChange,
					eventItemClick: this.eventClicked,
					viewEventClick: this.getInfo,
					viewEventText: "Info",
					viewEvent2Text: "Delete",
					viewEvent2Click: this.delete,
					updateEventStart: this.updateEventStart,
					updateEventEnd: this.updateEventEnd,
					moveEvent: this.moveEvent,
					newEvent: this.newEvent,
					nonAgendaCellHeaderTemplateResolver: this
					.nonAgendaCellHeaderTemplateResolver
				})),
			React.createElement("button", { onClick: this.write }, "Save Schedule"),
			React.createElement("button", { onClick: this.exports}, "Export Schedule"),
			React.createElement("button", { onClick: this.setLive}, "Set Live"),
			React.createElement("button", { onClick: this.setPSA}, "Set PSA"),
		); }
}

var _initialiseProps = function() {
	this.DIR = "/home/gem/code/projects/manager/src/";

	this.nonAgendaCellHeaderTemplateResolver = (
		schedulerData,
		item,
		formattedDateItems,
		style
	) => {
		let datetime = schedulerData.localeMoment(item.time);
		let isCurrentDate = false;

		if (schedulerData.viewType === ViewTypes.Day) {
			isCurrentDate = datetime.isSame(new Date(), "day");
		} else {
			isCurrentDate = datetime.isSame(new Date(), "hour");
		}

		if (isCurrentDate) {
			style.backgroundColor = "#118dea";
			style.color = "white";
		}

		return React.createElement(
			"th",
			{ key: item.time, className: `header3-text`, style: style },
			formattedDateItems.map((formattedItem, index) =>
				React.createElement("div", {
					key: index,
					dangerouslySetInnerHTML: {
						__html: formattedItem.replace(/[0-9]/g, "<b>$&</b>")
					}
				})
			)
		);
	};
	this.getInfo = (schedulerData,event) => {
		var result = this.state.fullData.find(obj => {
				return obj.id === event.id
		})
		// console.log(result)
		alert('id: ' + result.id)
		alert('start: ' + new Date(result.start))
		alert('end: ' + new Date(result.end))
		alert('Description: ' + result.desc)
		alert('Title: ' + result.title)
		alert('File Name: ' + result.path)
		alert('Duration: ' + Math.ceil(result.duration/60) + " mins, or " +  result.duration + " secs")
		alert('psa: ' + result.psa)
	}


	this.prevClick = schedulerData => {
		schedulerData.prev();
		// schedulerData.setEvents();
		this.setState({
			viewModel: schedulerData
		});
	};

	this.nextClick = schedulerData => {
		schedulerData.next();
		// schedulerData.setEvents(this.state.viewModel.getEvents());
		this.setState({
			viewModel: schedulerData
		});
	};

	this.onViewChange = (schedulerData, view) => {
		schedulerData.setViewType(
			view.viewType,
			view.showAgenda,
			view.isEventPerspective
		);
		// schedulerData.setEvents(this.state.viewModel.getEvents());
		this.setState({
			viewModel: schedulerData
		});
	};

	this.onSelectDate = (schedulerData, date) => {
		schedulerData.setDate(date);
		// schedulerData.setEvents(this.state.viewModel.getEvents());
		this.setState({
			viewModel: schedulerData
		});
	};

	this.eventClicked = (schedulerData, event) => {
		// alert(`id: ${event.id}}`);
	};

	this.edit = (schedulerData, event) => {
		var raw_start = prompt("Start Time");
		var raw_end = prompt("End Time");
	};

	this.delete = (schedulerData, event) => {
		schedulerData.setEvents(
			schedulerData.events.filter(function(e) {
				return e.id !== event.id;
			})
		);
		this.setState({
			fullData  : this.state.fullData.filter((e) => {e.id !== event.id}),
			viewModel : schedulerData
		});
		// function deleteByValue(val) {
		//     for(var f in fruits) {
		//         if(fruits[f] == val) {
		//             delete fruits[f];
		//         }
		//     }
		// }
	};

	this.getDuration = (fn) => {
		var s = 0;
		// console.log(this.state.path.substring(12))
		axios.get('/api/dur', {
			params: {
				file: this.state.path.substring(12)
			}
		}).catch((e) => console.log(e)).then((response) => {
			// handle success
			// console.log("get DUR")
			s = response.data.msg
			// console.log(s)
			fn(Math.ceil(s))
		});
		// setTimeout(()=>{
			// console.log('adfklsjfls' + Math.ceil(s))
			// fn(Math.ceil(s))
		// }, 5000)
	};
	this.getSDuration = (fn) => {

		// setTimeout(()=>{
			// console.log('dasffls')
			// return(Math.ceil(this.getDuration() / 60));
		// }, 6000)
		var s = 0
		this.getDuration((val) => {
			s = val
			// console.log(s)
			fn(Math.ceil(s/60) + "mins")
		})

	};

	this.findClosestStart = rStart => {
		var arr = this.state.fullData.map(function(item) {
			return this.state.fullData[item]["start"];
		});
		var timeStamps = arr.map(function(d) {
			return Date.parse(d);
		});
		return Math.min(
			...timeStamps.filter({
				funtion(d) {
					return d < rStart;
				}
			})
		);
	};

	this.findFurthestEnd = rEnd => {
		var arr = this.state.fullData.map(function(item) {
			return this.state.fullData[item]["end"];
		});
		var timeStamps = arr.map(function(d) {
			return Date.parse(d);
		});
		return Math.max(
			...timeStamps.filter({
				funtion(d) {
					return d < rEnd;
				}
			})
		);
	};

	this.canFit = (rStart, rEnd, duration) => {
		return rStart - rEnd >= duration;
	};

	this.adjFit = (
		closestStart,
		closestEnd,
		duration,
		containerStart,
		containerEnd
	) => {
		var i = 0;
		if (closestStart < containerStart) {
			++i;
		}

		if (closestEnd > containerEnd) {
			++i;
		}
		if (this.canFit(containerStart, containerEnd, duration) && i === 2) {
			return [containerStart, containerStart + duration];
		}

		var starts = this.state.fullData.map(function(item) {
			return this.state.fullData[item]["start"];
		});
		var ends = this.state.fullData.map(function(item) {
			return this.state.fullData[item]["end"];
		});
		starts.filter({
			function(start) {
				return start < containerStart && start > containerEnd;
			}
		});
		ends.filter({
			function(end) {
				return end < containerEnd && end > containerStart;
			}
		});
	};

	this.newEvent = (schedulerData, slotId, slotName, start, end, type, item) => {
		let newFreshId = 0;
		let ne = {};
		let t = this.state.title
		axios.get("/api/numg").then((response) => {
			newFreshId = response.data[0] + 1;
			// console.log("DONE")
			// console.log(newFreshId);

		}).catch(e => console.log(e + "e")).then(() => {
			axios.post("/api/nump", {
				id: newFreshId
			}).catch(e => console.log(e + "rr")).then((response) => {
				// console.log("in rr")
				// console.log(response)
				// handle success
				ne = {
					id: newFreshId,
					title: t,
					start: start,
					end: end,
					resourceId: slotId
				};

				// while(JSON.stringify(ne) === '{}'){
				//   console.log('waiting')
				// }

				schedulerData.addEvent(ne);
				// console.log(ne)
				this.setState({
					viewModel: schedulerData
				});
				this.genFullData(schedulerData,slotId,newFreshId, start, end)

			}).catch(e => console.log(e) + "O")
		}).catch(e => console.log(e) + "r")
	}
	this.setPSA = () => {
		var id = parseInt(prompt("id: "))
		var result = this.state.fullData.find(obj => {
			return obj.id === id
		})
		// console.log(result)
		if(isNaN(id) || result === undefined) {
			alert("INVALID ID")
		} else {
			result.psa = true
			var fd = this.state.fullData.filter((el) => el.id !== id).concat([result])
			this.setState({
				fullData: fd
			})
		}

	}
	this.setLive = () => {
		var id = parseInt(prompt("id: "))
		var type = prompt("live/livv: ")
		var duration = parseInt(prompt("duration(in seconds): "))
		if(isNaN(duration) && isNaN(id) && (type !== "live" && type !== "livv")) {
			alert("ERROR: Retry")
			// console.log(id)
			// console.log(duration)
		} else {

			var result = this.state.fullData.find(obj => {
				return obj.id === id
			})
			if(result !== undefined){
				if(result.start + duration*1000 < result.end) {
					result.path = type + ":"
					result.duration = duration
					var fd = this.state.fullData.filter((el) => el.id !== id).concat([result])
					this.setState({
						fullData: fd
					})
					// console.log(this.state.fullData)
				} else {
					alert("duration is too long")
				}
			}
			else {
				alert("id is not valid")
			}
		}
	}
	this.genFullData = (schedulerData,rid, id, cstart, cend) => {
		var d = {}
		var duration = 0
		var day = +new Date(schedulerData.selectDate).getDay()
		var path = this.state.path.substring(12)
		var end = 0
		// console.log('day')
		// console.log(day)
		this.getDuration((dur) => {
			duration = dur
			end = (+new Date(+new Date(cstart) + duration*1000)) + ((rid - day) * 86400000)
			d.psa = false
			d.id = id
			d.path = path
			d.start = (+new Date(cstart)) + ((rid - day) * 86400000)
			d.duration = duration
			d.cend = (+new Date(cend)) + ((rid - day) * 86400000)

			d.end = end
			d.title = this.state.title
			d.desc = this.state.desc
			if(!!this.state.title && !!end && !!cend && !!duration && !!path && !!cstart && id >= -1){

				this.state.fullData.push(d)
				// console.log(this.state.fullData)
			}
			else {
				alert("Please fill out everything")
			}

			// console.log(d)
		})
	}

	this.updateEventStart = (schedulerData, event, newStart) => {
		schedulerData.updateEventStart(event, newStart);
		this.setState({
			viewModel: schedulerData
		});
	};

	this.updateEventEnd = (schedulerData, event, newEnd) => {
		schedulerData.updateEventEnd(event, newEnd);
		this.setState({
			viewModel: schedulerData
		});
	};

	this.moveEvent = (schedulerData, event, slotId, slotName, start, end) => {
		schedulerData.moveEvent(event, slotId, slotName, start, end);
		this.setState({
			viewModel: schedulerData
		});
	};
	this.write = () => {
		console.log("writing")
		var sd = this.state.viewModel
		var data = sd.events
		// var day = +new Date(sd.selectDate).getDay()
		// for (var i = 0; i < data.length; i++) {
			// var e = data.length
			// var rid = e.resourceId
			// var m = 0
			// while(rid !== day) {
				// rid++
				// e.resourceId = rid
				// e.start += 86400000*m
				// e.end += 86400000*m
				// e.cend += 86400000*m
			// }
		// }
		axios.post("/api/datap", {
			data: data
		}).then(function(response) {
			// handle success
			// console.log(response);
		});

		axios.post("/api/fullp", {
			data: this.state.fullData
		}).then(function(response) {
			// handle success
			// console.log(response);
		});
	};
	this.exports = () => {
		var d = this.state.fullData
		d.sort((a,b) => +new Date(a.start) - +new Date(b.start))
		// console.log(d)
		var p = ""
		var date = ""
		var eArr = {}
		var length = d.length;
		for (var i = 0; i < length; i++) {
			date = new moment(d[i].start).format(DATE_FORMAT)
			if(!eArr[date]) {
				eArr[date] = []
			}
			eArr[date].push(d[i])
		}
		var slidesPath = prompt("Slides Path(ex: BusSponApril2018.mp4): ")
		var channel = prompt("Channel(ex: gov): ")
		// console.log(eArr)
		for (var date in eArr) {
			// console.log(date)
			var daySh = '#/bin/bash\n'
			var dayWeb = ""
			// console.log(eArr[date])
			for (var i = 0, len = eArr[date].length; i < len; i++) {
				var e = eArr[date][i]
				console.log(e)
				daySh += `/home/pi/$1 "${new moment(e.start).format("Y-MM-DD hh:mm:ss")}" "${e.path}" "${e.duration}"\n`
				if(eArr[date].length !== i+1){
					var slidesDur = (eArr[date][i+1].start - e.end)/1000
					daySh += `/home/pi/$1 "${new moment(e.end).format("Y-MM-DD hh:mm:ss")}" "${slidesPath}" "${slidesDur}"\n`
				}
				dayWeb += `${new moment(e.start).format("Y-MM-DD hh:mm:ss")}@${e.title}@${e.desc}@${e.duration}@${e.psa ? "PSA" : ""}\n`
			}
			// var slidesDur = e.end - e.cend
			// daySh += `\n/home/pi/$1 "${new moment(e.cstart).format("Y-M-D hh:mm:ss")}" "${e.path}" "${e.duration}"`
			// daySh += `\n/home/pi/$1 "${new moment(e.end).format("Y-M-D hh:mm:ss")}" "${slidesPath}" "${slidesDur}"

			daySh += `/home/pi/nextdayscript.sh "$1" "${new moment(date).add(1, 'days').format("Y-MM-DD")}"\n`
			console.log(dayWeb)
			console.log(daySh)

			axios.post("/api/exsh", {
				data: daySh,
				name: `${channel}${new moment(date).format("Y-MM-DD")}.sh`
			}).then(function(response) {
				// handle success
				console.log(response);
			});
			axios.post("/api/exweb", {
				data: dayWeb,
				name: `${channel}_${new moment(date).format("Y-MM-DD")}.txt`
			}).then(function(response) {
				// handle success
				console.log(response);
			});
		}
	}
}
export default withDragDropContext(Container);
