var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var fs = require('fs');

var app = express();

app.set('view engine', 'pug')
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// app.use(express.static(__dirname + '/public'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/api/numg', function(req, res) {
	//console.log(require('./number.json'))
	var fs  = require('fs')
	fs.readFile('./number.json', {encoding: 'utf-8'}, function(err,data){
		if (!err) {
			// console.log('received data: ' + JSON.parse(data));
			res.json(JSON.parse(data))
		} else {
			console.log(err);
		}})
	// res.json(require('./number.json'));
	// console.log(res.json)
})

app.get('/api/fullg', function(req, res) {

	fs.readFile('./full_data.json', {encoding: 'utf-8'}, function(err,data){
		if (!err) {
			// console.log('received data: ' + JSON.parse(data));
			res.json(JSON.parse(data))
		} else {
			console.log(err);
		}})
})

app.get('/api/datag', function(req, res) {

	fs.readFile('./data.json', {encoding: 'utf-8'}, function(err,data){
		if (!err) {
			// console.log('received data: ' + JSON.parse(data));
			res.json(JSON.parse(data))
		} else {
			console.log(err);
		}})
})
app.post('/api/nump', function(req, res) {
	try {
		var id = req.body.id

		fs.writeFile("number.json",new Buffer(JSON.stringify([ id ])), function(err) {
			if(err){
				console.log(err)
				throw err;
			}
			else{
				console.log("no err")
			}
			console.log("wrote")
			res.status(200).json({ msg: 'file written' })
			console.log("DONE WITH NUMP")
		})
	}
	catch(e) {
		console.log("ERROR!!!!!" + e)
	}
})

app.post('/api/exweb', function(req,res) {

	try {
		var data = req.body.data
		var name = req.body.name
		console.log(req)
		var WEBDIR = '/home/gem/code/projects/manager/server/web/'
		fs.writeFile(WEBDIR + name,new Buffer(data), function(err) {
			if(err){
				console.log(err)
				throw err;
			}
			else{
				console.log("no err")
			}
			console.log("wrote")
			res.status(200).json({ msg: 'file written' })
		})
	}
	catch(e) {
		console.log("error!!!!!" + e)
	}
})
app.post('/api/exsh', function(req,res) {

	try {
		var data = req.body.data
		var name = req.body.name
		console.log(req)
		var SHDIR = '/home/gem/code/projects/manager/server/sh/'
		fs.writeFile(SHDIR + name,new Buffer(data), function(err) {
			if(err){
				console.log(err)
				throw err;
			}
			else{
				console.log("no err")
			}
			console.log("wrote")
			res.status(200).json({ msg: 'file written' })
		})
	}
	catch(e) {
		console.log("error!!!!!" + e)
	}
})
app.post('/api/fullp', function(req, res) {
	try {

		var data = req.body.data
		// console.log(req)

		fs.writeFile("full_data.json",new Buffer(JSON.stringify( data )), function(err) {
			if(err){
				console.log(err)
				throw err;
			}
			else{
				console.log("no err")
			}
			console.log("wrote")
			res.status(200).json({ msg: 'file written' })
		})
	}
	catch(e) {
		console.log("error!!!!!" + e)
	}
})
app.post('/api/datap', function(req, res) {
	try {

		var data = req.body.data
		// console.log(req)

		fs.writeFile("data.json", new Buffer(JSON.stringify([ data ])), function(err) {
			if(err){
				console.log(err)
				throw err;
			}
			else{
				console.log("no err")
			}
			console.log("wrote")
			res.status(200).json({ msg: 'file written' })
		})
	}
	catch(e) {
		console.log("error!!!!!" + e)
	}
})
var DIR =  '/home/gem/code/projects/manager/src/'
app.get('/api/dur', function(req, res) {
	var file = req.query.file
	console.log(file)

	// console.log(DIR)
	var mp4duration = require("mp4duration");
	mp4duration.parse(DIR + file, function(err, d) {
		if(err){
			console.log("ESCAPE FOR ERROR, ERROR!!: " + err);
		}
		else{
			console.log(d);
			res.status(200).json({msg: d})
		}
	})
})

module.exports = app;
